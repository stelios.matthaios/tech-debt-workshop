using System.Collections;

namespace Homeguard
{
	public class CentralUnit
	{
		// sensor test status options
		public static readonly string PASS = "PASS";
		public static readonly string FAIL = "FAIL";
		public static readonly string PENDING = "pending";
		public static readonly string READY = "ready";

		private bool armed = false;
		private string securityCode;
		private IList sensors = new ArrayList();
		private IHomeguardView view = new TextView();
		private IAudibleAlarm audibleAlarm = new TextAudibleAlarm();

		// members to help with sensor tests
		private Hashtable sensorTestStatusMap;
		private bool runningSensorTest;
		private string sensorTestStatus;

		public bool IsArmed() {
			return armed;
		}

		public void Arm() {
			armed = true;
		}

		public void SetSecurityCode(string code) {
			securityCode = code;
		}

		public bool IsValidCode(string code) {
			return securityCode == code;
		}

		public void EnterCode(string code)
		{
			if(IsValidCode(code)) {
				armed = false;
				audibleAlarm.Silence();
			}
		}

		public bool AudibleAlarmIsOn() {
			return false;
		}

		public IList GetSensors() {
			return sensors;
		}

		public void RegisterSensor(Sensor sensor) {
			sensors.Add(sensor);
		}

		public void SetView(IHomeguardView view)
		{
			this.view = view;
		}

		public void SetAudibleAlarm(IAudibleAlarm alarm)
		{
			audibleAlarm = alarm;
		}

		public void ParseRadioBroadcast(string packet)
		{
			//parse the packet
			string[] tokens = packet.Split(",".ToCharArray());
			string id = tokens[0];
			string status = tokens[1];

			// find sensor with id
			Sensor sensor = null;
			foreach (Sensor s in sensors)
			{
				if(s.GetID() == id)
				{
					sensor = s;
					break;
				}
			}

			//trip or reset sensor
			if(sensor != null)
			{
				if("TRIPPED" == status)
					sensor.Trip();
				else
					sensor.Reset();
			}

			//get the message from the sensor and display it
			string message = GetSensorMessage(sensor);
			view.ShowMessage(message);

			// sound the alarm if armed
			if(IsArmed())
				audibleAlarm.Sound();

			// check if a sensor test is running and adjust status
			if(runningSensorTest)
			{
				if("TRIPPED" == status)
				{
					sensorTestStatusMap[id] = PASS;
				}

				// check to see if test is complete
				bool done = true;
				IDictionaryEnumerator myEnumerator = sensorTestStatusMap.GetEnumerator();
				while ( myEnumerator.MoveNext() ) {
					string testStatus = (string)myEnumerator.Value;
					if(PENDING == testStatus)
					{
						done = false;
						break;
					}
				}

				//terminate test if complete
				if(done)
					TerminateSensorTest();
			}
		}

		public void RunSensorTest()
		{
			runningSensorTest = true;
			sensorTestStatus = PENDING;

			// initialize the status map
			sensorTestStatusMap = new Hashtable();
			foreach(Sensor sensor in sensors) {
				sensorTestStatusMap[sensor.GetID()] = PENDING;
			}
		}

		// used during sensor test
		public void TerminateSensorTest()
		{
			runningSensorTest = false;

			// look at individual sensor status to determine the overall test status
			sensorTestStatus = PASS;
			IDictionaryEnumerator myEnumerator = sensorTestStatusMap.GetEnumerator();
			while ( myEnumerator.MoveNext() ) {
				string status = (string)myEnumerator.Value;
				if(status == PENDING)
				{
					sensorTestStatus = FAIL;
					break;
				}
			}
		}

		// used during sensor test
		public string GetSensorTestStatus()
		{
			return sensorTestStatus;
		}

		// used during sensor test
		public Hashtable GetSensorTestStatusMap()
		{
			return sensorTestStatusMap;
		}

		public string GetSensorMessage(Sensor sensor)
		{
			string message = "default";
			if(!sensor.IsTripped())
			{
				if(sensor.GetSensorType() == Sensor.DOOR)
					return sensor.GetLocation() + " is closed";
				else if(sensor.GetSensorType() == Sensor.WINDOW)
					return sensor.GetLocation() + " is sealed";
				else if(sensor.GetSensorType() == Sensor.MOTION)
					return sensor.GetLocation() + " is motionless";
				else if(sensor.GetSensorType() == Sensor.FIRE)
					return sensor.GetLocation() + " temperature is normal";
			}
			else
			{
				if(sensor.GetSensorType() == Sensor.DOOR)
					return sensor.GetLocation() + " is open";
				else if(sensor.GetSensorType() == Sensor.WINDOW)
					return sensor.GetLocation() + " is ajar";
				else if(sensor.GetSensorType() == Sensor.MOTION)
					return "Motion detected in " + sensor.GetLocation();
				else if(sensor.GetSensorType() == Sensor.FIRE)
					return sensor.GetLocation() + " is on FIRE!";
			}
			return message;
		}		
	}
}
