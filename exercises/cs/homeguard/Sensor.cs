using System;

namespace Homeguard
{
	/// <summary>
	/// Summary description for Sensor.
	/// </summary>
	public class Sensor
	{
		public const string DOOR = "door";
		public const string MOTION = "motion";
		public const string WINDOW = "window";
		public const string FIRE = "fire";

		private string id;
		private string location;
		private string type;
		private bool tripped = false;

		public Sensor(string id, string location, string type)
		{
			this.id = id;
			this.location = location;
			this.type = type;
		}

		public String GetID()
		{
			return id;
		}

		public String GetSensorType()
		{
			return type;
		}

		public String GetLocation()
		{
			return location;
		}

		public void Trip()
		{
			tripped = true;
		}

		public void Reset()
		{
			tripped = false;
		}

		public bool IsTripped()
		{
			return tripped;
		}
	}
}
